from enum import IntEnum
import logging
from colorama import Fore, Back, Style
import random
import sys
from typing import Iterator

def is_android():
    return hasattr(sys, 'getandroidapilevel')
    #return platform.system() == 'Linux' and platform.machine() == 'aarch64'

class Point:
    def __init__(self, x: int | str, y: int = None) -> None:
        if type(x) == str:
            x = x.split(' ')
            self.x = int(x[0])
            self.y = int(x[1])
        else:
            self.x = x
            self.y = y

    def __len__(self) -> int:
        return 2
    
    def __iter__(self) -> Iterator[int]:
        yield self.x
        yield self.y
    
    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y)
    
    def __repr__(self) -> str:
        return f"({self.x}, {self.y})"
    
    def __abs__(self):
        return Point(abs(self.x), abs(self.y))

class Login:
    def __init__(self, username: str, password: str) -> None:
        self.username = username
        self.password = password

class OP(IntEnum):
    #https://git.pixelplanet.fun/ppfun/pixelplanet/src/branch/master/src/socket/packets/op.js
    ChangeCanvas= 0xA0
    RegisterChunk= 0xA1
    UnregisterChunk= 0xA2
    REG_MCHUNKS_OP = 0xA3
    DEREG_MCHUNKS_OP = 0xA4
    ChangeMe= 0xA6
    OnlineCounter= 0xA7
    Heartbeat= 0xB0
    Place= 0xC1
    Cooldown= 0xC2
    PlaceResponse= 0xC3
    CHUNK_UPDATE_MB_OP = 0xC4
    PIXEL_UPDATE_MB_OP = 0xC5
    CaptchaResponse= 0xC6

class PlaceResponse(IntEnum):
    #https://git.pixelplanet.fun/ppfun/pixelplanet/src/branch/master/src/ui/PixelTransferController.js
    Success= 0x00
    InvalidCanvas= 0x01
    InvalidCoordinateX= 0x02
    InvalidCoordinateY= 0x03
    InvalidCoordinateZ= 0x04
    InvalidColor= 0x05
    LoginRequired= 0x06
    NotEnoughScore= 0x07
    Protected= 0x08
    Cooldown= 0x09
    Captcha= 0x0A
    ProxyDetected= 0x0B
    NotInTop10= 0x0C
    ServerConfused= 0x0D
    IPBanned= 0x0E
    IPRangeBanned= 0x0F
    Timeout= 0x10

class Strategy(IntEnum):
    NtS= 1
    StN= 2
    WtE= 3
    EtW= 4
    Snail= 5
    DiagSnail= 6
    Random= 7

def NtS(h: int, w: int) -> Iterator[Point]:
    p = Point(0, 0)
    for p.y in range(h):
        for p.x in range(w):
            yield p

def StN(h: int, w: int) -> Iterator[Point]:
    p = Point(w-1, h-1)
    for p.y in range(h-1, -1, -1):
        for p.x in range(w-1, -1, -1):
            yield p

def WtE(h: int, w: int) -> Iterator[Point]:
    p = Point(0, 0)
    for p.x in range(w):
        for p.y in range(h):
            yield p

def EtW(h: int, w: int) -> Iterator[Point]:
    p = Point(w-1, h-1)
    for p.x in range(w-1, -1, -1):
        for p.y in range(h-1, -1, -1):
            yield p

def random_point(h: int, w: int) -> Iterator[Point]:
    while True:
        yield Point(random.randint(0, w-1), random.randint(0, h-1))

def snail_line(h: int, w: int) -> Iterator[Point]:
    step = 1
    p = Point(w//2, h//2)
    yield p

    while True:
        for _ in range(step): #right
            p.x += 1
            yield p

        for _ in range(step): #down
            p.y += 1
            yield p

        step += 1

        for _ in range(step): #left
            p.x -= 1
            yield p

        for _ in range(step): #up
            p.y -= 1
            yield p

        step += 1

def diagonal_snail_line(h: int, w: int) -> Iterator[Point]:
    step = 1
    p = Point(w//2, h//2)
    yield p

    while True:
        #right
        p.x += 1
        yield p

        for _ in range(step-1): #down-right
            p.y += 1
            p.x += 1
            yield p

        for _ in range(step): #down-left
            p.y += 1
            p.x -= 1
            yield p

        for _ in range(step): #up-left
            p.y -= 1
            p.x -= 1
            yield p

        for _ in range(step): #up-right
            p.y -= 1
            p.x += 1
            yield p

        step += 1

class CustomFormatter(logging.Formatter):
    format = "%(asctime)s - {0}%(levelname)s{1} - %(message)s"

    FORMATS = {
        logging.DEBUG: format.format(Back.LIGHTBLACK_EX, Style.RESET_ALL),
        logging.INFO: format.format(Back.BLUE, Style.RESET_ALL),
        logging.WARNING: format.format(Back.YELLOW, Style.RESET_ALL),
        logging.ERROR: format.format(Back.RED, Style.RESET_ALL),
        logging.CRITICAL: format.format(Back.RED + Style.BRIGHT, Style.RESET_ALL)
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)

logger = logging.getLogger("ppfun")
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

ch.setFormatter(CustomFormatter())

logger.addHandler(ch)