## Installation
- On Windows: 
    - Install [Python 3](https://www.python.org/downloads/)
    - Install [GTK runtime library](https://github.com/tschoonj/GTK-for-Windows-Runtime-Environment-Installer/releases/)
- On Android: 
    - Download [Termux](https://f-droid.org/packages/com.termux/) and [Termux:API](https://f-droid.org/packages/com.termux.api/)
    - Inside Termux: `pkg update && pkg install openssl python termux-api libjpeg-turbo libcairo`
    - Optionally install git: `pkg install git`
    - Clone the repo under `/sdcard` otherwise the captcha image in the termux notification will not be shown
- Clone the repository: `git clone https://gitlab.com/zelea/ppfun.git` or download the source
- Navigate to the project directory: `cd ppfun`
- Install dependencies: `pip install -r requirements.txt`

## Usage
See `python ppfun.py -h`  
On Android press `ACQUIRE WAKELOCK` in the Termux notification to not stop when your phone is in idle.