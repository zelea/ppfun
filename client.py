import asyncio
import json
import os

import aiohttp
from PIL import Image
from cairosvg import svg2png
import aiofiles

import twocaptcha

from classes import *

if not is_android():
    import _tkinter
    from tkinter import Tk, Canvas, Entry, StringVar, TclError
    from PIL import ImageTk

class Client:
    def __init__(self, login_data: Login | None = None, proxy: str | None = None) -> None:
        self.headers= {'User-Agent': 'Copium', 'Origin': "https://pixelplanet.fun"}
        self.proxy = proxy
        self.cooldown = 0
        self.stop = False
        self.login_data = login_data
        self.twocaptcha = twocaptcha.ApiClient(os.getenv('APIKEY_2CAPTCHA'))

    async def __aexit__(self, *excinfo) -> None:
        if hasattr(self, 'wssession') and not self.wssession.closed:
            self.ws_reader_task.cancel()
            self.heartbeat_task.cancel()
            await self.wssession.close()

        if hasattr(self, 'httpsession') and not self.httpsession.closed:
            await self.httpsession.close()

    def list_canvases(self) -> None:
        for id, canvas in self.me['canvases'].items():
            print(f"{id}: {canvas['title']}")

    @property
    async def _me(self) -> dict:
        async with self.httpsession.get('https://pixelplanet.fun/api/me') as response:
            if response.status != 200:
                raise aiohttp.ClientResponseError(request_info="Getting user info", history=tuple(), status=response.status, message='Response not 200 OK')
            return await response.json()
    
    async def connect(self) -> None:
        self.httpsession = aiohttp.ClientSession(headers= self.headers)
        if self.login_data:
            token = await self.login(self.login_data)
            self.headers['Cookies'] = f"pixelplanet.session={token}"
            self.httpsession.headers = self.headers
        self.me = await self._me
        self.wssession = await self.httpsession.ws_connect("wss://pixelplanet.fun/ws", proxy=self.proxy) 
        self.heartbeat_task = asyncio.create_task(self.heartbeat())
        self.ws_reader_task = asyncio.create_task(self.handle_websocket_messages())
    
    @staticmethod
    def euclidean_distance(color1: tuple, color2: tuple) -> float:
        assert len(color1) == len(color2) == 3
        return (sum((a - b) ** 2 for a, b in zip(color1, color2))) ** 0.5
    
    @staticmethod
    async def show_image(image: str) -> str | None:
        if is_android():
            os.system(f'termux-notification -i 1488 -c "Enter CAPTCHA verification" --sound --image-path {image} --action "am start --user 0 -n com.termux/.app.TermuxActivity"')
        else:
            with Image.open(image) as pil_image:
                window = Tk()
                window.resizable(False, False)
                window.title("CAPTCHA")

                canvas = Canvas(window, width=pil_image.width, height=pil_image.height)
                canvas.pack()

                tk_image = ImageTk.PhotoImage(pil_image)
                canvas.create_image(0, 0, anchor="nw", image=tk_image)

                entry = Entry(window, justify= 'center', font=("Courier New", 12), validatecommand= lambda x: len(x)<=4, width= 4)
                entry.pack(pady=10)

                result = StringVar()
                def enter(event):
                    result.set(entry.get())
                    window.destroy()
                
                window.bind("<Return>", enter)
                window.focus_set()
                entry.focus_set()

                async def main_loop():
                    while True:
                        while window.dooneevent(_tkinter.DONT_WAIT) > 0:
                            pass
                        
                        try:
                            window.winfo_exists()
                        except TclError:
                            break
                        await asyncio.sleep(0.1)
                    
                await main_loop()

                return result.get()
            
    async def heartbeat(self):
        while True:
            await asyncio.sleep(30)
            await self.wssession.send_bytes(bytes(OP.Heartbeat))
            logger.debug("Heartbeat sent")

    async def resolve_captcha(self) -> None:
        r = await self.httpsession.get("https://pixelplanet.fun/captcha.svg")
        svg2png(await r.content.read(), write_to= "captcha.png")
        cid = r.headers['captcha-id']
        async with aiofiles.open("captcha.png", 'rb') as f:
            result = await self.twocaptcha.solve_image_to_text(await f.read(), phrase= False, case= False, numeric= 0, math= False, minLength= 4, maxLength= 4, comment= "not case-sensitive, can contain numbers and letters")
            text = result['solution']['text']
            self.task_id = result['taskId']
        #text = await self.show_image("captcha.png")
        #if not text:
        #    text = await ainput("captcha: ")
        await self.wssession.send_str(f'cs,["{text}","{cid}"]')

    async def handle_websocket_messages(self):
        async for message in self.wssession:
            if type(message.data) == str:
                chat = json.loads(message.data[3:])
                #print(f"@[{chat[0]}]({chat[4]}) on {[None, 'en', 'int', None, 'art'][chat[3]]} ({chat[2]}): {chat[1]}")
            else:
                match message.data[0]:
                    case OP.OnlineCounter:
                        logger.info(f"People online: {int.from_bytes(message.data[1:3])}")
                    case OP.Cooldown:
                        wait = int.from_bytes(message.data[1:5])/1000
                        logger.info(f"Total cooldown: {wait} s")
                        #self.cooldown = wait
                    case OP.Place:
                        chunk_x = message.data[1]
                        chunk_y = message.data[2]
                        offset_z = message.data[3]
                        offset_y = message.data[4]
                        offset_x = message.data[5]
                        color = message.data[6]

                        x = chunk_x * 256 + offset_x
                        y = chunk_y * 256 + offset_y
                        self.canvas[x][y] = color
                        logger.info(f'Pixel placed at ({x}, {y}) with colour {color}')
                    case OP.CaptchaResponse:
                        if not message.data[1]:
                            self.stop = False
                            await self.twocaptcha.report(self.task_id, True)
                            logger.info("CAPTCHA verification successful")
                        else:
                            self.stop = False
                            await self.twocaptcha.report(self.task_id, False)
                            logger.warning("CAPTCHA verification failed")
                    case OP.PlaceResponse:
                        match message.data[1]:
                            case PlaceResponse.Success:
                                cooldown = int.from_bytes(message.data[2:6])/1000
                                wait = int.from_bytes(message.data[6:8])
                                logger.info(f"Cooldown for {cooldown}+{wait}s")
                                #self.cooldown = cooldown
                                self.stop = False
                            case PlaceResponse.Protected:
                                logger.warning("Pixel is protected")
                            case PlaceResponse.Cooldown:
                                cds = self.me['canvases'][str(self.active_canvas)]['cds']/1000
                                pcd = self.me['canvases'][str(self.active_canvas)]['pcd']/1000
                                logger.warning(f"Time limit reached ({cds} s)")
                                self.cooldown = cds-pcd
                            case PlaceResponse.ProxyDetected:
                                logger.warning("Your IP is detected as a proxy")
                            case PlaceResponse.IPBanned:
                                logger.warning("Your IP has been banned")
                            case PlaceResponse.IPRangeBanned:
                                logger.warning("Your IP has been banned")
                            case PlaceResponse.InvalidColor:
                                logger.warning("Invalid color")
                            case PlaceResponse.Captcha:
                                self.stop = True
                                asyncio.create_task(self.resolve_captcha())
                            case _:
                                logger.warning(f"Unknown place response code: {message.data}")
                    case _:
                        logger.warning(f"Unknown OPCode: {message.data}")
        
        self.heartbeat_task.cancel()
        await self.heartbeat_task

    def render_chunk_data(self, data: bytes) -> Image:
        image = Image.new("RGB", (256, 256))
        for y in range(256):
            for x in range(256):
                color = data[y*256+x]
                if color >= 128:
                    color-=128
                image.putpixel((x, y), tuple(self.me['canvases']['0']['colors'][color]))

        return image
    
    async def get_chunk_data(self, canvas: int, chunk: Point) -> bytes:
        response = await self.httpsession.get(f'https://pixelplanet.fun/chunks/{canvas}/{chunk.x}/{chunk.y}.bmp')
        if response.status != 200:
            raise aiohttp.ClientResponseError(request_info="Getting user info", history=tuple(), status=response.status, message='Response not 200 OK')
        else:
            return await response.content.read()

    async def render_canvas(self, canvas: int) -> Image:
        size = self.me['canvases'][str(canvas)]['size']
        image = Image.new("RGB", (size, size))

        for y in range(size//256):
            for x in range(size//256):
                image.paste(self.render_chunk_data(await self.get_chunk_data(canvas, Point(x,y))), (x*256, y*256, (x+1)*256, (y+1)*256))

        return image

    async def register_chunk(self, chunk: Point) -> None:
        await self.wssession.send_bytes(bytes([OP.RegisterChunk, chunk.x, chunk.y]))
        logger.debug(f"Subscribed to {chunk} chunk updates")

    async def change_canvas(self, canvas: int) -> None:
        await self.wssession.send_bytes(bytes([OP.ChangeCanvas, canvas]))
        self.active_canvas= canvas
        logger.debug(f"Active canvas changed to {canvas}")

    async def login(self, login: Login) -> None:
        logger.info("logging in...")
        async with self.httpsession.post('https://pixelplanet.fun/api/auth/local', json={
            'nameoremail': self.login_data.username,
            'password': self.login_data.password
        }) as response:
            if response.json()['success']:
                return response.cookies.get('pixelplanet.session')
            else:
                logger.error("Failed to log in")

    def color2code(self, canvas: int, color: tuple[int]) -> int:
        color = color[:3]
        colors = self.me['canvases'][str(canvas)]['colors']
        cli = self.me['canvases'][str(canvas)]['cli']

        return min(
            list(enumerate(colors))[cli:], 
            key= lambda x: self.euclidean_distance(x[1], color)
            )[0]

    async def place(self, canvas: int, coord: Point, color: tuple[int]) -> bool:
        if len(color) == 4 and color[3] == 0: #alpha
            return False

        color = color[:3]

        color = self.color2code(canvas, color)
        colors = self.me['canvases'][str(canvas)]['colors']

        canvas_size = self.me['canvases'][str(canvas)]['size']

        x = coord.x + canvas_size//2
        y = coord.y + canvas_size//2
        if self.euclidean_distance(colors[self.canvas[x][y]], colors[color]) == 0: #same color
            return False
        chunk_x = x // 256
        offset_x = x % 256
        chunk_y = y // 256
        offset_y = y % 256

        data = [
            OP.Place,
            chunk_x,
            chunk_y,
            0,
            offset_y,
            offset_x,
            color
        ]
        await self.wssession.send_bytes(bytes(data))
        logger.info(f"Placing pixel to ({coord.x}, {coord.y}) with colour {color}")
        return True

    async def draw(self, image: Image, canvas: int, coord: Point, strategy: Strategy = Strategy.Random, dither: Image.Dither = Image.Dither.NONE) -> None:
        await self.change_canvas(canvas)

        size = self.me['canvases'][str(canvas)]['size']
        palette = self.me['canvases'][str(canvas)]['colors']
        #image = image.quantize(method= dither, palette= palette)

        self.canvas = {}

        for chunk_y in range((coord.y + size//2)//256, (coord.y + size//2+image.height)//256+1):
            for chunk_x in range((coord.x + size//2)//256, (coord.x + size//2+image.width)//256+1):
                chunk = await self.get_chunk_data(canvas, Point(chunk_x, chunk_y))
                for offset_x in range(256):
                    for offset_y in range(256):
                        if self.canvas.get(chunk_x*256+offset_x) == None:
                            self.canvas[chunk_x*256+offset_x]= {}
                        self.canvas[chunk_x*256+offset_x][chunk_y*256+offset_y]= chunk[offset_x+offset_y*256]
                await self.register_chunk(Point(chunk_x, chunk_y))

        canvas_size = self.me['canvases'][str(canvas)]['size']
        #fine = 0
        #for y in range(image.height):
        #    for x in range(image.width):
        #        if image.getpixel((x,y))[3] == 0 or self.color2code(canvas, image.getpixel((x,y))) == self.canvas[coord.x+canvas_size//2+x][coord.y+canvas_size//2+y]:
        #            fine += 1

        #logger.info(f"Progress: {fine/(image.height*image.width)*100}%")

        match strategy:
            case Strategy.NtS:
                genertor = NtS(image.height, image.width)
            case Strategy.StN:
                genertor = NtS(image.height, image.width)
            case Strategy.WtE:
                genertor = NtS(image.height, image.width)
            case Strategy.EtW:
                genertor = NtS(image.height, image.width)
            case Strategy.Snail:
                genertor = snail_line(image.height, image.width)
            case Strategy.DiagSnail:
                genertor = diagonal_snail_line(image.height, image.width)
            case Strategy.Random:
                genertor = random_point(image.height, image.width)

        while True:
            if self.cooldown: logger.debug(f"Waiting for {self.cooldown} s")
            await asyncio.sleep(self.cooldown)
            self.cooldown = 0

            while self.stop:
                await asyncio.sleep(0.1)
            for p in genertor:
                if await self.place(canvas= canvas, coord= coord+ p, color= image.getpixel((p.x, p.y))): await asyncio.sleep(1)

    async def start(self, image: Image, canvas: int, coord: Point, strategy: Strategy) -> None:
        await self.connect()
        self.draw_task = asyncio.create_task(self.draw(image, canvas, coord, strategy))
        await asyncio.gather(self.ws_reader_task, self.heartbeat_task, self.draw_task)