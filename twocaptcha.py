import aiohttp
from base64 import b64encode
import asyncio

ERROR_SUCCESS                   = 0
ERROR_KEY_DOES_NOT_EXIST        = 1
ERROR_NO_SLOT_AVAILABLE         = 2
ERROR_ZERO_CAPTCHA_FILESIZE     = 3
ERROR_TOO_BIG_CAPTCHA_FILESIZE  = 4
ERROR_ZERO_BALANCE              = 10
ERROR_IP_NOT_ALLOWED            = 11
ERROR_CAPTCHA_UNSOLVABLE        = 12
ERROR_BAD_DUPLICATES            = 13
ERROR_NO_SUCH_METHOD            = 14
ERROR_IMAGE_TYPE_NOT_SUPPORTED  = 15
ERROR_NO_SUCH_CAPCHA_ID         = 16
ERROR_IP_BLOCKED                = 21
ERROR_TASK_ABSENT               = 22
ERROR_TASK_NOT_SUPPORTED        = 23
ERROR_RECAPTCHA_INVALID_SITEKEY = 31
ERROR_ACCOUNT_SUSPENDED         = 55
ERROR_BAD_PROXY                 = 130
ERROR_BAD_PARAMETERS            = 110
ERROR_BAD_IMGINSTRUCTIONS       = 115

class ApiClient():
    def __init__(self, api_key: str, *, soft_id: int = None, callback: str = None, api_server: str = 'api.2captcha.com', lang: str = None) -> None:
        self.api_url = f"https://{api_server}"
        self.api_key = api_key
        self.soft_id = soft_id
        self.callback = callback
        self.lang = lang
    
    async def _request(self, name: str, **kwargs) -> dict:
        async with aiohttp.ClientSession(self.api_url) as session:
            kwargs['clientKey'] = self.api_key
            async with session.post('/'+name, json= kwargs) as response:
                if response.status != 200:
                    raise aiohttp.ClientResponseError(request_info="2captcha", history=tuple(), status=response.status, message='Response not 200 OK')
                response = await response.json()
                if response['errorId'] != ERROR_SUCCESS:
                    raise Exception(response['errorId'], response['errorDescription'])
                return response

    async def _create_task(self, task: dict, **kwargs) -> int:
        kwargs['task'] = task
        return (await self._request('createTask', **kwargs))['taskId']
    
    async def _report_correct(self, task_id: int) -> None:
        await self._request('reportCorrect', taskId= task_id)

    async def _report_incorrect(self, task_id: int) -> None:
        await self._request('reportIncorrect', taskId= task_id)

    async def _get_task_result(self, task_id: int) -> bool | dict:
        result = await self._request('getTaskResult', taskId= task_id)
        if result['status'] == "processing":
            return False
        else:
            return result
    
    @property
    async def balance(self) -> float:
        return (await self._request('getBalance'))['balance']

    async def report(self, task_id: int, correct: bool) -> None:
        if correct:
            await self._report_correct(task_id)
        else:
            await self._report_incorrect(task_id)

    async def _create_image_to_text_task(self, image: bytes, **kwargs) -> int:
        kwargs['type']= "ImageToTextTask"
        kwargs['body']= b64encode(image).decode('utf-8')
        
        return await self._create_task(kwargs)
    
    async def solve_image_to_text(self, image: bytes, **kwargs) -> str:
        task_id = await self._create_image_to_text_task(image, **kwargs)

        while not (task_result := await self._get_task_result(task_id)):
            await asyncio.sleep(1)
        
        task_result['taskId'] = task_id
        return task_result