#!/usr/bin/env python3
import asyncio
import aiohttp
import argparse
from PIL import Image
from classes import *
from client import *
from colorama import init
from dotenv import load_dotenv

load_dotenv()

parser = argparse.ArgumentParser(
                    prog='ppfun',
                    description='Automated bot for pixelplanet.fun',
                    epilog='I wonder if this bot could place 6 million pixels in just 5 years... 🤔')

parser.add_argument('-i', "--image", 
                    type= Image.open, 
                    required= False,
                    metavar= "PATH",
                    help= "Image to draw")
parser.add_argument('-p', "--proxy", 
                    type= str, 
                    required= False,
                    metavar= "TYPE://USER:PASS@IP:PORT",
                    help= "Proxy url")
parser.add_argument('-l', "--login", 
                    type= Login, 
                    required= False,
                    metavar= "LOGIN",
                    help= "login details")
parser.add_argument('-c', "--coord", 
                    type= Point, 
                    required= False,
                    metavar= "X Y",
                    help= "top left pixel of the drawing")
parser.add_argument('-C', "--canvas", 
                    type= int, 
                    required= False,
                    metavar= "N",
                    help= "canvas")
parser.add_argument('-s', '--strategy',
                    type= int,
                    required= False,
                    metavar= 'N',
                    help= "strategy of drawing: 1: North to South, 2: South to North, 3: West to East, 4: East to West, 5: Snail, 6: Diagonal snail, 7: Random",
                    default= Strategy.Random
                    )
parser.add_argument('-d', '--dither',
                    type= int,
                    required= False,
                    metavar= 'N',
                    help= "strategy of dithering: 0: None, 1: Ordered, 2: Rasterize, 3: Floyd-Steinberg",
                    default= Image.Dither.NONE
                    )
parser.add_argument('-L', '--list-canvas',
                    action= 'store_true',
                    help= "List avalible canvases")

args = parser.parse_args()

init(autoreset=True)

def main():
    while True:
        try:
            client = Client(args.login, args.proxy)
            asyncio.run(client.start(args.image, args.canvas, args.coord, args.strategy))
        except (ConnectionResetError, aiohttp.client_exceptions.ClientConnectorError, asyncio.exceptions.CancelledError, aiohttp.client_exceptions.WSServerHandshakeError, aiohttp.client_exceptions.ClientResponseError, aiohttp.client_exceptions.ServerDisconnectedError, aiohttp.client_exceptions.ClientOSError, aiohttp.client_exceptions.ClientPayloadError) as e:
            asyncio.run(client.__aexit__())
            del client
            logger.error(e)
            logger.warning("Connection closed. Reconnecting...")

if __name__ == "__main__":
    main()
